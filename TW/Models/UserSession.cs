﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TW.Constants;

namespace TW.Models {
    public class UserSession {
        public int Id { get; set; }
        public string Identity { get; set; }
        public bool IsWebIdentity { get; set; }
        public string DisplayableName { get; set; }
        public Roles Roles { get; set; }
        public bool IsAdministrator { get; set; }
    }
}