﻿using System;

namespace TW.Models.Database {
    public class UserLessons {
        public virtual int Id { get; protected set; }
        public virtual User User { get; set; }
        public virtual int ExperiencePoints { get; set; }
        public virtual Lesson Lesson { get; set; }
        public virtual DateTime? AddDate { get; set; }
    }
}