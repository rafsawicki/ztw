﻿using System;

namespace TW.Models.Database
{
    public class Category
    {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual int Lft { get; set; }
        public virtual int Rgt { get; set; }
        public virtual bool Visible { get; set; }
        public virtual DateTime? AddDate { get; set; }
    }
}