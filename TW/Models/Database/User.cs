﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TW.Constants;

namespace TW.Models.Database {
    public class User {
        public virtual int Id { get; protected set; }
        public virtual string Identity { get; set; }
        public virtual byte[] PasswordKey { get; set; }
        public virtual byte[] PasswordSalt { get; set; }

        public virtual string DisplayableName { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Email { get; set; }

        public virtual int ExperiencePoints { get; set; }

        public virtual Roles Roles { get; set; }
        public virtual bool IsAdministrator { get; set; }
        public virtual bool IsWebIdentity { get; set; }
        public virtual bool IsEnabled { get; set; }

        public virtual string AvatarPath { set; get; }

        public virtual IList<UserAchievement> UserAchievements { get; protected set; }

        public User() {
            IsEnabled = true;
        }
    }
}