﻿using System;

namespace TW.Models.Database {
    public class Exercise {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string Content { get; set; }
        public virtual int ExperiencePoints { get; set; }
        public virtual bool IsAccepted { get; set; }
        public virtual Lesson Lesson { get; set; }
        public virtual DateTime? AddDate { get; set; }
        public virtual int Status { get; set; }
        public virtual ExerciseType Type { get; set; }
        public virtual string Answer { get; set; }
        public virtual string Question { get; set; }
        public virtual string Help { get; set; }
    }
}