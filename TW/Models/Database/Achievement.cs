﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TW.Models.Database {
    public class Achievement {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual string ImagePath { get; set; }
        public virtual IList<UserAchievement> UserAchievements { get; protected set; } 
    }
}