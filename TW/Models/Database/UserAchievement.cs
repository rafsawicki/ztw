﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TW.Models.Database {
    public class UserAchievement {
        public virtual int Id { get; protected set; }
        public virtual User User { get; set; }
        public virtual Achievement Achievement { get; set; }
        public virtual DateTime ObtainDate { get; set; }
    }
}