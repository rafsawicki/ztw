﻿using System;

namespace TW.Models.Database {
    public class Lesson {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string Content { get; set; }
        public virtual string MainImage { get; set; }
        public virtual int ExperiencePoints { get; set; }
        public virtual bool IsAccepted { get; set; }
        public virtual Category Category { get; set; }
        public virtual DateTime? AddDate { get; set; }
        public virtual int Status { get; set; }
    }
}