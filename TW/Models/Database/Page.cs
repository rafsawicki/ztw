﻿using System;

namespace TW.Models.Database
{
    public class Page
    {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string Content { get; set; }
        public virtual int Position { get; set; }
        public virtual bool Visible { get; set; }
        public virtual DateTime? EditDate { get; set; }
        public virtual Lesson Lesson { get; set; }
    }
}