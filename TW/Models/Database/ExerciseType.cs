﻿using System;

namespace TW.Models.Database
{
    public class ExerciseType
    {
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual string Content { get; set; }
        public virtual bool Visible { get; set; }
        public virtual DateTime? AddDate { get; set; }
    }
}