﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TW.Models.ViewModels {
    public class PasswordChangeModel {
        [Display(Name = "Dotychczasowe hasło")]
        [Required(ErrorMessage = "Musisz podać hasło.")]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Hasło musi być dłuższe niż 4 znaki i krótsze niż 50 znaków.")]
        public string OldPassword { get; set; }

        [Display(Name = "Nowe hasło")]
        [Required(ErrorMessage = "Musisz podać hasło.")]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Hasło musi być dłuższe niż 4 znaki i krótsze niż 50 znaków.")]
        public string NewPassword { get; set; }

        [Display(Name = "Powtórz hasło")]
        [Required(ErrorMessage = "Musisz podać hasło.")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Podane hasła różnią się.")]
        public string PasswordRepeat { get; set; } 
    }
}