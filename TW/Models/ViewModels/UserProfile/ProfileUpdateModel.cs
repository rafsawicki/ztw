﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TW.Models.ViewModels {
    public class ProfileUpdateModel {
        [Display(Name = "Wyświetlana nazwa")]
        [Required(ErrorMessage = "Musisz podać wyświetlaną nazwę.")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Wyświetlana nazwa musi być krótsza niż 50 znaków.")]
        public string DisplayableName { get; set; }

        [Display(Name = "Imię")]
        [StringLength(255, ErrorMessage = "Imię musi być krótsze niż 255 znaków.")]
        public string FirstName { get; set; }

        [Display(Name = "Nazwisko")]
        [StringLength(255, ErrorMessage = "Nazwisko musi być krótsze niż 255 znaków.")]
        public virtual string LastName { get; set; }

        [Display(Name = "Adres e-mail")]
        [StringLength(255, ErrorMessage = "Adres e-mail musi być krótszy niż 255 znaków.")]
        [RegularExpression(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$", ErrorMessage = "Adres e-mail ma niepoprawny format.")]
        public virtual string Email { get; set; }
    }
}