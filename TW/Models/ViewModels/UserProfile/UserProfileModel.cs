﻿namespace TW.Models.ViewModels {
    public class UserProfileModel {
        public string DisplayableName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string AvatarPath { get; set; }
        public int CurrentLevel { get; set; }
        public int NextLevelProgressPercent { get; set; }
        public int ExperiencePoints { get; set; }
        public int AchievementCount { get; set; }
        public int MessageCount { get; set; }
    }
}