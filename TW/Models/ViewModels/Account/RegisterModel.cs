﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TW.Models.ViewModels {
    public class RegisterModel {
        [Display(Name = "Nazwa użytkownika")]
        [Required(ErrorMessage = "Musisz podać nazwę użytkownika.")]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Nazwa użytkownika musi być dłuższa niż 4 znaki i krótsza niż 50 znaków.")]
        public string Username { get; set; }

        [Display(Name = "Hasło")]
        [Required(ErrorMessage = "Musisz podać hasło.")]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Hasło musi być dłuższe niż 4 znaki i krótsze niż 50 znaków.")]
        public string Password { get; set; }

        [Display(Name = "Powtórz hasło")]
        [Required(ErrorMessage = "Musisz podać hasło.")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Podane hasła różnią się.")]
        public string PasswordRepeat { get; set; }

        [Display(Name = "Wyświetlana nazwa")]
        [Required(ErrorMessage = "Musisz podać wyświetlaną nazwę.")]
        [StringLength(50, ErrorMessage = "Nazwa wyświetlana musi być krótsza niż 50 znaków.")]
        public string DisplayableName { get; set; }
    }
}