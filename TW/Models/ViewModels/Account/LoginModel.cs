﻿using System.ComponentModel.DataAnnotations;

namespace TW.Models.ViewModels {
    public class LoginModel {
        [Display(Name = "Nazwa użytkownika")]
        [Required(ErrorMessage = "Musisz podać nazwę użytkownika.")]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Nazwa użytkownika musi być dłuższa niż 4 znaki i krótsza niż 50 znaków.")]
        public string Username { get; set; }

        [Display(Name = "Hasło")]
        [Required(ErrorMessage = "Musisz podać hasło.")]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 4, ErrorMessage = "Hasło musi być dłuższe niż 4 znaki i krótsze niż 50 znaków.")]
        public string Password { get; set; }

        [Display(Name = "Zapamiętaj mnie?")]
        public bool RememberMe { get; set; }
    }
}