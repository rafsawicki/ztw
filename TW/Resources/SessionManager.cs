﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Linq;
using NHibernate.Tool.hbm2ddl;
using TW.Mappings;

namespace TW.Resources {
    public static class SessionManager {
        private static Lazy<ISessionFactory> _session = new Lazy<ISessionFactory>(CreateSessionFactory);

        public static ISessionFactory SessionFactory {
            get { return _session.Value; }
        }

        private static ISessionFactory CreateSessionFactory() {
            return Fluently.Configure()
                    .Database(MySQLConfiguration.Standard.ConnectionString(
                        c => c.FromConnectionStringWithKey("MySQLConnectionString")
                                  )
                    )
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<LessonMap>())
                    //Odtworzenie bazy - ta metoda usuwa istniejace tabele na podstawie obecnej konfiguracji
//                    .ExposeConfiguration(config => new SchemaExport(config).Drop(false, true))
                    //
//                    .ExposeConfiguration(config => new SchemaUpdate(config).Execute(false, true))
                    .BuildSessionFactory();
        }

        public static ISession OpenSession() {
            return _session.Value.OpenSession();
        }

        public static ISession CurrentSession() {
            return _session.Value.GetCurrentSession();
        }

        public static void InTransaction(Action<ISession> operation) {
            using (var session = OpenSession()) {
                using (var tx = session.BeginTransaction()) {
                    operation(session);
                    tx.Commit();
                }
            }
        }
    }
}