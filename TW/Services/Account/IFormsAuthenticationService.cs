﻿using System.Collections.Generic;
using DotNetOpenAuth.AspNet;
using TW.Models.ViewModels;

namespace TW.Services.Account {
    public interface IFormsAuthenticationService {
        void RegisterUser(RegisterModel registerModel);
        void WebIdentitySignIn(AuthenticationResult result, bool rememberMe);
        void UserSignIn(LoginModel loginModel);
        void SignOut();
        IDictionary<string, string> ValidateLoginModel(LoginModel loginModel);
        IDictionary<string, string> ValidateRegisterModel(RegisterModel registerModel);
    }
}