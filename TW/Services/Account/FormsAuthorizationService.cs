﻿using TW.Constants;
using TW.Models;

namespace TW.Services.Account {
    public class FormsAuthorizationService : IAuthorizationService {
        public bool Authorize(UserSession userSession, Roles requiredRoles) {
            if (userSession.IsAdministrator) {
                return true;
            } else {
                return (requiredRoles & userSession.Roles) == requiredRoles;
            }
        }
    }
}