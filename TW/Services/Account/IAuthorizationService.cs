﻿using TW.Constants;
using TW.Models;

namespace TW.Services.Account {
    public interface IAuthorizationService {
        bool Authorize(UserSession userSession, Roles requiredRoles);
    }
}