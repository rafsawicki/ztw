﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using NHibernate;
using TW.Models.Database;
using TW.Models.ViewModels;
using TW.Resources;
using Roles = TW.Constants.Roles;
using TW.Common;

namespace TW.Services.Account {
    public class FormsAuthenticationService : IFormsAuthenticationService {
        private const string DefaultAvatarPath = "http://lorempixel.com/250/250/cats/Avatar/";

        public void RegisterUser(RegisterModel registerModel) {
            byte[] passwordSalt;
            byte[] passwordKey = GetPasswordKeySalt(registerModel.Password, out passwordSalt);
            User newUser = new User() {
                Identity = registerModel.Username,
                PasswordKey = passwordKey,
                PasswordSalt = passwordSalt,
                DisplayableName = registerModel.DisplayableName,
                Roles = Roles.Student,
                AvatarPath = DefaultAvatarPath
            };
            SessionManager.InTransaction(session => session.Save(newUser));
        }

        public void WebIdentitySignIn(AuthenticationResult result, bool rememberMe) {
            //            TODO: Upewnic sie, ze zwykla nazwa uzytkownika nie zawiera +
            string identity = result.Provider + "+" + result.ProviderUserId;
            User user = null;
            SessionManager.InTransaction(session => {
                user = session.QueryOver<User>().WithIdentity(identity).SingleOrDefault();
                if (user == null) {
                    user = new User() {
                        Identity = identity,
                        DisplayableName = result.UserName,
                        Roles = Roles.Student,
                        IsWebIdentity = true,
                        AvatarPath = DefaultAvatarPath
                    };
                    session.Save(user);
                }
            });
           
            SignIn(user, rememberMe);
        }

        public void UserSignIn(LoginModel loginModel) {
            User requestedUser = GetUserWithIdentity(loginModel.Username);
            if (requestedUser == null || requestedUser.IsWebIdentity) return;

            if (IsValidPassword(loginModel.Password, requestedUser.PasswordKey, requestedUser.PasswordSalt)) {
                SignIn(requestedUser, loginModel.RememberMe);
            }
        }

        private void SignIn(User user, bool rememberMe) {
            FormsAuthentication.SetAuthCookie(user.Identity, rememberMe);
            HttpContext.Current.User.ReloadUserSession();
        }

        public bool ChangePassword(string oldPassword, string newPassword) {
            bool success = false;
            SessionManager.InTransaction(session => {
                User currentUser = session.Get<User>(HttpContext.Current.User.UserSession().Id);
                if (IsValidPassword(oldPassword, currentUser.PasswordKey, currentUser.PasswordSalt)) {
                    byte[] passwordSalt;
                    byte[] passwordKey = GetPasswordKeySalt(newPassword, out passwordSalt);
                    currentUser.PasswordSalt = passwordSalt;
                    currentUser.PasswordKey = passwordKey;
                    success = true;
                }
            });
            return success;
        }

        public void SignOut() {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
        }

        public IDictionary<string, string> ValidateLoginModel(LoginModel loginModel) {
            var errors = new Dictionary<string, string>();

            User user = GetUserWithIdentity(loginModel.Username);
            if (user == null) {
                errors.Add("Username", "Użytkownik o podanej nazwie nie istnieje.");
            } else if (user.IsWebIdentity) {
                errors.Add("Username", "Nie można zalogować się przy użyciu podanej nazwy użytkownika.");
            } else if (!IsValidPassword(loginModel.Password, user.PasswordKey, user.PasswordSalt)) {
                errors.Add("Password", "Podane hasło jest nieprawidłowe.");
            }
            return errors;
        }

        public IDictionary<string, string> ValidateRegisterModel(RegisterModel registerModel) {
            var errors = new Dictionary<string, string>();

            User user = GetUserWithIdentity(registerModel.Username);
            if (user != null) errors.Add("Username", "Użytkownik o podanej nazwie już istnieje.");
            return errors;
        }


        public static bool IsValidPassword(string password, byte[] passwordKey, byte[] passwordSalt) {
            using (var deriveBytes = new Rfc2898DeriveBytes(password, passwordSalt)) {
                byte[] newKey = deriveBytes.GetBytes(20);  // derive a 20-byte key
                return newKey.SequenceEqual(passwordKey);
            }
        }

        private byte[] GetPasswordKeySalt(string password, out byte[] salt) {
            using (var deriveBytes = new Rfc2898DeriveBytes(password, 20)) {
                salt = deriveBytes.Salt;
                byte[] key = deriveBytes.GetBytes(20);  // derive a 20-byte key
                return key;
            }
        }

        private User GetUserWithIdentity(string identity) {
            User requestedUser = null;
            SessionManager.InTransaction(session => requestedUser = session.QueryOver<User>().Where(user => user.Identity == identity).SingleOrDefault());
            return requestedUser;
        }
    }
}