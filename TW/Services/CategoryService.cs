﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TW.Models;
using TW.Models.Database;
using TW.Resources;
using NHibernate.Linq;

namespace TW.Services {
    public static class CategoryService {
        public static IList<Category> GetByName(string name) {
            IList<Category> category = null;
            SessionManager.InTransaction(session => {
                category = session.QueryOver<Category>()
                    .Where(c => c.Name == name && c.Visible == true)
                    .List();
            });
            return category;
        }

        public static IList<Category> GetAllLeafNodes()
        {
            IList<Category> category = null;
            SessionManager.InTransaction(session =>
            {
                category = session.QueryOver<Category>()
                    .Where(c => c.Lft + 1 == c.Rgt && c.Visible == true)
                    .List();
            });
            return category;
        }

        public static IList<Category> GetLeafNodesFromCategory(int id)
        {
           
            IList<Category> categories = null;
            SessionManager.InTransaction(session =>
            {
                Category category = session.Get<Category>(id);
                categories = session.QueryOver<Category>()
                    .Where(c => c.Lft + 1 == c.Rgt && c.Visible == true && c.Lft > category.Lft && c.Rgt < category.Rgt)
                    .List();
            });
            return categories;
        }

        public static void SetCategoryInToCategory(int id, string name)
        {
            SessionManager.InTransaction(session =>
            {
                Category category = session.Get<Category>(id);
                IList<Category> categories = session.QueryOver<Category>()
                                                    .Where(c => c.Lft > category.Lft || c.Rgt > category.Lft)
                                                    .List();
                Category newCategory = new Category();
                newCategory.Lft = category.Lft + 1;
                newCategory.Rgt = category.Lft + 2;
                newCategory.Name = name;
                newCategory.Visible = true;
                newCategory.AddDate = DateTime.Now;
                session.Save(newCategory);
                foreach (var cat in categories)
                {
                    if (cat.Lft > category.Lft)
                        cat.Lft += 2;
                    if (cat.Rgt > category.Lft)
                        cat.Rgt += 2;
                }
            });
        }

        public static void DeleteById(int id)
        {
            SessionManager.InTransaction(session =>
            {
                Category category = session.Get<Category>(id);
                IList<Category> categories = session.QueryOver<Category>()
                                                    .Where(c => c.Lft > category.Lft || c.Rgt > category.Lft)
                                                    .List();
                category.Visible = false;
                category.Lft = 0;
                category.Rgt = 0;
                session.Update(category);
                foreach (var cat in categories)
                {
                    if (cat.Lft > category.Lft)
                        cat.Lft -= 2;
                    if (cat.Rgt > category.Lft)
                        cat.Rgt -= 2;
                }
            });
        }

        public static Category GetById(int id)
        {
            Category category = null;
            SessionManager.InTransaction(session =>
            {
                category = session.Get<Category>(id);
            });
            return category;
        }

        public static IList<Category> GetAll() {
            IList<Category> category = null;
            SessionManager.InTransaction(session => {
                category = session.QueryOver<Category>()
                    .OrderBy(c => c.Lft).Asc
                    .List();
            });
            return category;
        }

        public static IList<Category> GetAllVisible()
        {
            IList<Category> category = null;
            SessionManager.InTransaction(session =>
            {
                category = session.QueryOver<Category>()
                    .Where(c => c.Visible == true)
                    .List();
            });
            return category;
        }
    }
}