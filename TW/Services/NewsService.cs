﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TW.Models;
using TW.Models.Database;
using TW.Resources;
using NHibernate.Linq;

namespace TW.Services {
    public static class NewsService {
        public static IList<News> GetNewestSix()
        {
            IList<News> news = null;
            SessionManager.InTransaction(session =>
            {
                news = session.QueryOver<News>()
                    .Where(l => l.Visible == true)
                    .OrderBy(l => l.Id).Desc
                    .Take(6)
                    .List();
            });
            return news;
        } 
    }
}