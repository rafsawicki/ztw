﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using TW.Models;
using TW.Models.Database;
using TW.Models.ViewModels;
using TW.Resources;
using TW.Services.Account;
using TW.Common;

namespace TW.Services {
    public class UserProfileService {
        public UserProfileModel GetCurrentUserProfileDetails() {
            UserProfileModel userProfileModel = new UserProfileModel();
            User currentUser = null;
            SessionManager.InTransaction(session => {
                currentUser = session.Get<User>(HttpContext.Current.User.UserSession().Id);
                userProfileModel.AchievementCount = currentUser.UserAchievements.Count;

                //TODO: Getting message count, exp for next level
            });
            userProfileModel.DisplayableName = currentUser.DisplayableName;
            userProfileModel.FirstName = currentUser.FirstName;
            userProfileModel.LastName = currentUser.LastName;
            userProfileModel.Email = currentUser.Email;
            userProfileModel.ExperiencePoints = currentUser.ExperiencePoints;
            userProfileModel.AvatarPath = currentUser.AvatarPath;

            var res = GetLevelAndRemainingToNext(currentUser.ExperiencePoints);
            userProfileModel.CurrentLevel = res.Item1;
            userProfileModel.NextLevelProgressPercent = res.Item2;
            return userProfileModel;
        }

        private Tuple<int, int> GetLevelAndRemainingToNext(int exp) {
            int firstLevelExp = 50;
            int currentLevelExp = firstLevelExp;
            int level = 0;
            while (exp > currentLevelExp) {
                level++;
                currentLevelExp *= 2;
            }
            int prevLvlExp = currentLevelExp/2;
            double percent = currentLevelExp != firstLevelExp ? ((double)exp - prevLvlExp)/(currentLevelExp - prevLvlExp) : (double)exp/firstLevelExp;
            return Tuple.Create(level, (int)(percent*100));
        }

        public ProfileUpdateModel GetPrefilledUpdateModel() {
            User currentUser = null;
            SessionManager.InTransaction(session => {
                currentUser = session.Get<User>(HttpContext.Current.User.UserSession().Id);
            });
            return new ProfileUpdateModel() {
                DisplayableName = currentUser.DisplayableName,
                Email = currentUser.Email,
                FirstName = currentUser.FirstName,
                LastName = currentUser.LastName
            };
        }

        public IDictionary<string, string> ValidatePasswordChangeModel(PasswordChangeModel passwordModel) {
            var errors = new Dictionary<string, string>();

            User user = null;
            SessionManager.InTransaction(session => user = session.Get<User>(HttpContext.Current.User.UserSession().Id));
            if (!FormsAuthenticationService.IsValidPassword(passwordModel.OldPassword, user.PasswordKey, user.PasswordSalt)) {
                errors.Add("Password", "Podane hasło jest nieprawidłowe.");
            }
            return errors;
        }

        public bool UpdateProfile(ProfileUpdateModel updateModel) {
            bool success = false;
            SessionManager.InTransaction(session => {
                User user = session.Get<User>(HttpContext.Current.User.UserSession().Id);
                user.FirstName = updateModel.FirstName;
                user.LastName = updateModel.LastName;
                user.DisplayableName = updateModel.DisplayableName;
                user.Email = updateModel.Email;
                success = true;
            });
            return success;
        }

        public static void SaveUserProgres(int exp, int lesson)
        {
            SessionManager.InTransaction(session => {
                User user = session.Get<User>(HttpContext.Current.User.UserSession().Id);
                user.ExperiencePoints += exp;
                Lesson l = session.Get<Lesson>(lesson);
                UserLessons ul = new UserLessons()
                    {
                        Lesson = l,
                        User = user,
                        ExperiencePoints = exp
                    };
                session.Save(ul);
            });
        }

        public static int GetExpFromCategory(int category)
        {

            IList<Lesson> lesson = null;
            SessionManager.InTransaction(session =>
            {

                lesson = session.QueryOver<Lesson>()
                    .Where(l => l.Category.Id == category)
                    .List();
            });
            IList<UserLessons> ul = null;
            SessionManager.InTransaction(session =>
            {
                User user = session.Get<User>(HttpContext.Current.User.UserSession().Id);
                    ul = session.QueryOver<UserLessons>()
                                .Where(l => lesson.Contains(l.Lesson) && l.User.Id == user.Id)
                                .List();
                });
            int exp = 0;
            foreach (UserLessons u in ul)
            {
                exp += u.ExperiencePoints;
            }
            return exp;
        }
    }
}