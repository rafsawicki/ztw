﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TW.Models;
using TW.Models.Database;
using TW.Resources;
using NHibernate.Linq;

namespace TW.Services {
    public static class LessonService {
        public static Lesson GetLesson(int lessonId) {
            Lesson lesson = null;
            SessionManager.InTransaction(session => {
                lesson = session.Get<Lesson>(lessonId);
            });
            return lesson;
        }

        public static IList<Lesson> GetAllLessons() {
            IList<Lesson> lessons = null;
            SessionManager.InTransaction(session => {
                lessons = session.QueryOver<Lesson>()
                    .Where(l => l.Status == 0)
                    .OrderBy(l => l.AddDate).Desc
                    .List();
            });
            return lessons;
        }

        public static IList<Lesson> Search(string query)
        {
            IList<Lesson> lessons = null;
            SessionManager.InTransaction(session =>
            {
                lessons = session.QueryOver<Lesson>()
                    .Where(l => l.Status == 0)
                    .WhereRestrictionOn(l => l.Name).IsLike("%"+query+"%")
                    .OrderBy(l => l.AddDate).Desc
                    .List();
            });
            return lessons;
        } 

        public static IList<Lesson> GetNewestThree()
        {
            IList<Lesson> lessons = null;
            SessionManager.InTransaction(session =>
            {
                lessons = session.QueryOver<Lesson>()
                    .Where(l => l.Status == 0)
                    .OrderBy(l => l.Id).Desc
                    .Take(3)
                    .List();
            });
            return lessons;
        } 

        public static IList<Lesson> GetAllFromCategory(int category)
        {
            IList<Lesson> lessons = null;
            SessionManager.InTransaction(session =>
            {
                lessons = session.QueryOver<Lesson>()
                    .Where(l => l.Category.Id == category && l.Status == 0)
                    .OrderBy(l => l.AddDate).Desc
                    .List();
            });
            return lessons;
        }

        public static IList<Page> getPagesFromLesson(int lesson)
        {
            IList<Page> pages = null;
            SessionManager.InTransaction(session =>
            {
                pages = session.QueryOver<Page>()
                                .Where(p => p.Lesson.Id == lesson && p.Visible == true)
                                .OrderBy(p => p.Position).Asc
                                .List();
            });
            return pages;
        }

        public static IList<Exercise> getExercisesFromLesson(int lesson)
        {
            IList<Exercise> exercises = null;
            SessionManager.InTransaction((session =>
                {
                    exercises = session.QueryOver<Exercise>()
                                       .Where(e => e.Lesson.Id == lesson)
                                       .OrderBy(e => e.Id).Asc
                                       .List();
                }));
            return exercises;
        }
    }
}