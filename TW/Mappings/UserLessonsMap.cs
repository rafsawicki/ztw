﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models;
using TW.Models.Database;

namespace TW.Mappings {
    public class UserLessonsMap : ClassMap<UserLessons> {
        public UserLessonsMap()
        {
            Id(x => x.Id);
            References(x => x.User);
            Map(x => x.ExperiencePoints).Not.Nullable();
            Map(x => x.AddDate);
            References(x => x.Lesson);
        }
    }
}