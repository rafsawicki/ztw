﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models.Database;

namespace TW.Mappings {
    public class UserAchievementMap : ClassMap<UserAchievement> {
        public UserAchievementMap() {
            Id(x => x.Id);
            Map(x => x.ObtainDate);
            References(x => x.User);
            References(x => x.Achievement);
/*            Table("user_achievements");
            Id(x => x.Id).Column("id_user_achievement");
            Map(x => x.ObtainDate).Column("obtain_date");
            References(x => x.User).Column("id_user").ForeignKey("id_user");
            References(x => x.Achievement).Column("id_achievement").ForeignKey("id_user");*/
        }

    }
}