﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models.Database;

namespace TW.Mappings {
    public class PageMap : ClassMap<Page> {
        public PageMap() {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Content).Length(100000);
            Map(x => x.Position);
            Map(x => x.Visible).Not.Nullable();
            Map(x => x.EditDate);
            References(x => x.Lesson);
        }
    }
}