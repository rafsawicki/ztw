﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models.Database;

namespace TW.Mappings {
    public class AchievementMap : ClassMap<Achievement> {
        public AchievementMap() {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Description).Length(10000);
            Map(x => x.ImagePath);
            HasMany(x => x.UserAchievements).Inverse();
        }
    }
}