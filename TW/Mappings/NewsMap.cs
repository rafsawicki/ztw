﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models.Database;

namespace TW.Mappings {
    public class NewsMap : ClassMap<News> {
        public NewsMap() {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Content).Length(100000);
            Map(x => x.Visible).Not.Nullable();
            Map(x => x.BackgroundUrl);
            Map(x => x.AddDate);
        }
    }
}