﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models;
using TW.Models.Database;

namespace TW.Mappings {
    public class ExerciseMap : ClassMap<Exercise> {
        public ExerciseMap() {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Content).Length(1000000);
            Map(x => x.ExperiencePoints).Not.Nullable();
            Map(x => x.IsAccepted).Not.Nullable();
            Map(x => x.AddDate);
            References(x => x.Lesson);
            Map(x => x.Status);
            References(x => x.Type);
            Map(x => x.Answer).Length(10000);
            Map(x => x.Question).Length(10000);
            Map(x => x.Help).Length(10000);
        }
    }
}