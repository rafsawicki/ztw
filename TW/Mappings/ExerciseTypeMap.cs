﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models.Database;

namespace TW.Mappings {
    public class ExerciseTypeMap : ClassMap<ExerciseType> {
        public ExerciseTypeMap() {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Content);
            Map(x => x.Visible).Not.Nullable();
            Map(x => x.AddDate);
        }
    }
}