﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Constants;
using TW.Models.Database;

namespace TW.Mappings {
    public class UserMap : ClassMap<User> {
        public UserMap() {
            Id(x => x.Id);
            Map(x => x.Identity).Not.Nullable().Unique();
            Map(x => x.PasswordKey);
            Map(x => x.PasswordSalt);
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.Email);
            Map(x => x.DisplayableName).Not.Nullable().Default("");
            Map(x => x.ExperiencePoints).Not.Nullable().Default("0");
            Map(x => x.Roles).CustomType<Roles>().Not.Nullable().Default("0");
            Map(x => x.IsAdministrator).Not.Nullable().Default("0");
            Map(x => x.IsWebIdentity).Not.Nullable().Default("0");
            Map(x => x.IsEnabled).Not.Nullable().Default("1");
            Map(x => x.AvatarPath);
            HasMany(x => x.UserAchievements).Inverse().Cascade.All();
        }
    }
}