﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models;
using TW.Models.Database;

namespace TW.Mappings {
    public class LessonMap : ClassMap<Lesson> {
        public LessonMap() {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Content);
            Map(x => x.MainImage);
            Map(x => x.ExperiencePoints).Not.Nullable();
            Map(x => x.IsAccepted).Not.Nullable();
            Map(x => x.AddDate);
            References(x => x.Category);
            Map(x => x.Status);
/*
            Table("lessons");
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Content);
            Map(x => x.ExperiencePoints).Not.Nullable();
            Map(x => x.IsAccepted).Not.Nullable();
            Map(x => x.AddDate);
*/

        }
    }
}