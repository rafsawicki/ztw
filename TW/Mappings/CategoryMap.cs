﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Mapping;
using TW.Models.Database;

namespace TW.Mappings {
    public class CategoryMap : ClassMap<Category> {
        public CategoryMap() {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Lft).Not.Nullable();
            Map(x => x.Rgt).Not.Nullable();
            Map(x => x.Visible).Not.Nullable();
            Map(x => x.AddDate);
        }
    }
}