﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TW.Common;
using TW.Models;
using TW.Models.Database;
using TW.Services;

namespace TW.Controllers
{
    public class LessonController : Controller
    {
        public ActionResult Details(int id = 0, int page = 1)
        {
            Lesson lesson = null;
            IList<Page> pages = null;
            IList<Exercise> exercises = null;
            lesson = Services.LessonService.GetLesson(id);
            pages = Services.LessonService.getPagesFromLesson(id);
            exercises = Services.LessonService.getExercisesFromLesson(id);

            if (pages.Count > 0)
            {
                ViewBag.lesson = lesson;
                ViewBag.page = pages[page-1];
                ViewBag.pagesNumb = pages.Count;
                ViewBag.currentPage = page;
                ViewBag.ex = exercises.Count;
            } 
            ViewBag.View = "_Page";
            if (Request.IsAjaxRequest())
            {
                if (ViewBag.lesson != null && ViewBag.page != null) return PartialView("_Details");
                Response.StatusCode = 404;
                return Json(JsonResponseFactory.SuccessResponse("Podana lekcja nie posiada obecnie treści"));
            }
            return View("_Details");
        }

        public ActionResult Index(string view, int id = 0, int page = 1)
        {
            Lesson lesson = null;
            IList<Page> pages = null;
            lesson = Services.LessonService.GetLesson(id);
            pages = Services.LessonService.getPagesFromLesson(id);
            if (pages.Count > 0)
            {
                ViewBag.lesson = lesson;
                ViewBag.page = pages[page];
                ViewBag.pagesNumb = pages.Count;
                ViewBag.currentPage = page;
            }
            ViewBag.View = "_Page";
            if (pages.Count == 0) {
                return RedirectToAction("Index", "Category");
            }
            if (Request.IsAjaxRequest()) return PartialView("_Details");
            return View("_Details");
        }

        /*//
        // GET: /Lesson/

        public ActionResult DisplayLesson()
        {
            Lesson l = new Lesson();
            l.AddDate = new DateTime(2012, 10, 10);
            l.Content = "test";
            return View("DisplayLesson", l);
//            return View("DisplayLesson");
        }*/

//        public ActionResult AddLess
    }
}
