﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using TW.Common;
using TW.Constants;
using TW.Filters;
using TW.Models.Database;
using TW.Models.ViewModels;
using TW.Resources;
using TW.Services;
using TW.Services.Account;
using NHibernate.Linq;

namespace TW.Controllers
{
    [Authorize]
    public class UserProfileController : Controller {
        private readonly UserProfileService _userProfileService;
        private readonly FormsAuthenticationService _formsAuthenticationService;
        private readonly Random _random = new Random();

        public UserProfileController() {
            _userProfileService = new UserProfileService();
            _formsAuthenticationService = new FormsAuthenticationService();
            ViewBag.ProfileDetails = _userProfileService.GetCurrentUserProfileDetails();
            ViewBag.UpdateModel = _userProfileService.GetPrefilledUpdateModel();
        }

        public ActionResult Index(string view) {
            ViewBag.View = "_" + (view ?? "Skills");
            if (view == "AddLesson") CreateCategoryList();
            if (view == "Achievements") CreateAchievementList();
            if (view == null || view == "Skills") InitCategories();
            if (Request.IsAjaxRequest()) return PartialView("Index");
            return View("Index");
        }

        public ActionResult Inbox() {
            if (Request.IsAjaxRequest()) return PartialView("_Inbox");
            return Index("Inbox");
        }

        public ActionResult Edit() {
            if (Request.IsAjaxRequest()) return PartialView("_Edit");
            return Index("Edit");
        }

        public ActionResult Skills() {
            InitCategories();
            if (Request.IsAjaxRequest()) return PartialView("_Skills");
            return Index("Skills");
        }

        public ActionResult Achievements() {
            CreateAchievementList();
            if (Request.IsAjaxRequest()) return PartialView("_Achievements");
            return Index("Achievements");
        }

        public ActionResult AddLesson() {
            CreateCategoryList();
            if (Request.IsAjaxRequest()) return PartialView("_AddLesson");
            return Index("AddLesson");
        }

        public void InitCategories() {
            IList<Category> categories = Services.CategoryService.GetAll();
            int[] nodesWithDepth = new int[categories.Max(category1 => category1.Id) + 1];
            int[] expForCategory = new int[categories.Max(category1 => category1.Id) + 1];
            string[] skillLevel = new string[categories.Max(category1 => category1.Id) + 1];
            foreach (Category category in categories) {
                int counter = 0;
                foreach (Category category2 in categories) {
                    if (category2.Lft < category.Lft && category2.Rgt > category.Rgt)
                        counter++;
                }
                nodesWithDepth[category.Id] = counter;
                expForCategory[category.Id] = GetExpForCategoryPlaceholder(category.Id);
                skillLevel[category.Id] = GetSkillLevelForCategoryPlaceholder(category.Id, expForCategory[category.Id]);
            }
            ViewBag.categories = categories;
            ViewBag.order = nodesWithDepth;
            ViewBag.exp = expForCategory;
            ViewBag.skills = skillLevel;
        }

        private string GetSkillLevelForCategoryPlaceholder(int id, int i) {
            return "Początkujący";
        }

        private int GetExpForCategoryPlaceholder(int id) {
            return _random.Next(100);
        }

        public ActionResult AddExercise()
        {
            IList<string> types = null;
            SessionManager.InTransaction(session =>
            {
                types = session.Query<ExerciseType>()
                    .Select(category => category.Name)
                    .ToList();
            });
            IList<string> lessons = null;
            SessionManager.InTransaction(session =>
            {
                lessons = session.Query<Lesson>()
                    .Select(lesson => lesson.Name)
                    .ToList();
            });
            ViewBag.ExerciseTypeList = new SelectList(types);
            ViewBag.LessonsList = new SelectList(lessons);
            if (Request.IsAjaxRequest()) return PartialView("_AddExercise");
            return Index("AddExercise");
        }

        private void CreateCategoryList() {
            IList<string> categories = null;
            SessionManager.InTransaction(session => {
                categories = session.Query<Category>().Select(category => category.Name).ToList();
            });
            ViewBag.CategoryList = new SelectList(categories);
        }

        public void CreateAchievementList() {
            IList<Achievement> achievements = null;
            SessionManager.InTransaction(session => {
                achievements =
                    session.Query<UserAchievement>()
                           .Where(achievement => achievement.User.Id == User.UserSession().Id)
                           .Select(achievement => achievement.Achievement).ToList();
            });
            ViewBag.AchievementList = achievements;
        }

#if DEBUG
        private void SpawnAchievement(ISession session, string description, string name, string imagePath) {
            Achievement test = new Achievement() {
                Description = description,
                Name = name,
                ImagePath = imagePath
            };
            session.Save(test);
            User current = session.Get<User>(User.UserSession().Id);
            UserAchievement ua = new UserAchievement() {
                Achievement = test,
                User = current,
                ObtainDate = DateTime.Now
            };
            session.Save(ua);
        }
#endif

        [HttpPost]
        public ActionResult UpdateProfile(ProfileUpdateModel updateModel) {
            if (ModelState.IsValid) {
                bool success = _userProfileService.UpdateProfile(updateModel);
                if (success) return Json(JsonResponseFactory.SuccessResponse());
            }
            return
                Json(JsonResponseFactory.ErrorResponse(JsonHelper.GetModelErrorMessages(ModelState)));
        }

        [HttpPost]
        public ActionResult ChangePassword(PasswordChangeModel passwordChangeModel) {
            ModelState.Merge(_userProfileService.ValidatePasswordChangeModel(passwordChangeModel), "");
            if (ModelState.IsValid) {
                bool success = _formsAuthenticationService.ChangePassword(passwordChangeModel.OldPassword,
                                                           passwordChangeModel.NewPassword);
                if (success) return Json(JsonResponseFactory.SuccessResponse());
            }
            return Json(JsonResponseFactory.ErrorResponse(JsonHelper.GetModelErrorMessages(ModelState)));
        }

        [HttpPost]
        public ActionResult UploadFiles(IEnumerable<HttpPostedFileBase> files) {
            if (files.Any()) {
                var file = files.First();
                string fileName = User.UserSession().Id + Path.GetExtension(file.FileName);
                var dirPath = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Content\\images\\avatar\\");
                string filePath = Path.Combine(dirPath, fileName);
                if (!Directory.Exists(dirPath)) Directory.CreateDirectory(dirPath);
                System.IO.File.WriteAllBytes(filePath, ReadData(file.InputStream));

                SessionManager.InTransaction(session => {
                    var user = session.Get<User>(User.UserSession().Id);
                    user.AvatarPath = "/Content/images/avatar/"+fileName;
                });

                return Json(JsonResponseFactory.SuccessResponse());
            }
            return Json(JsonResponseFactory.ErrorResponse("Upload obrazka nie powiódł się."));
        }

        private byte[] ReadData(Stream stream) {
            byte[] buffer = new byte[16 * 1024];

            using (MemoryStream ms = new MemoryStream()) {
                int read;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0) {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SubmitLesson(FormCollection formCollection) {
            SessionManager.InTransaction(session => {
                Category cat =
                    session.Query<Category>()
                           .FirstOrDefault(category => category.Name == formCollection["CategoryName"]);
                Lesson l = new Lesson() {
                    AddDate = DateTime.Now,
                    Name = formCollection["LessonName"],
                    Category = cat,
                    Content = formCollection["LessonDescription"],
                    MainImage = formCollection["ImagePath"],
                    ExperiencePoints = int.Parse(formCollection["ExpPoints"]),
                    IsAccepted = true
                };
                session.Save(l);
                string[] pageNames = Request.Form.GetValues("PageName");
                string[] pageContents = Request.Form.GetValues("content");
                for (int i = 0; i < pageNames.Length; i++) {
                    Page p = new Page() {
                        Content = pageContents[i],
                        EditDate = DateTime.Now,
                        Lesson = l,
                        Name = pageNames[i],
                        Position = i+1,
                        Visible = true,
                    };
                    session.Save(p);
                }
            });
            return Index("AddLesson");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SubmitExercise(FormCollection formCollection)
        {
            SessionManager.InTransaction(session =>
            {
                Lesson lesson =
                    session.Query<Lesson>()
                           .FirstOrDefault(l => l.Name == formCollection["LessonName"]);
                ExerciseType exType =
                    session.Query<ExerciseType>()
                           .FirstOrDefault(e => e.Name == formCollection["TypeName"]);
                Exercise exercise = new Exercise()
                {
                    AddDate = DateTime.Now,
                    Name = formCollection["ExerciseName"],
                    Lesson = lesson,
                    Type = exType,
                    Content = formCollection["ExerciseDescription"],
                    Question = formCollection["QuestionDescription"],
                    Answer = formCollection["AnswerDescription"],
                    Help = formCollection["HelpDescription"],
                    ExperiencePoints = int.Parse(formCollection["ExpPoints"]),
                    IsAccepted = true
                };
                session.Save(exercise);
            });
            return Index("AddExercise");
        }
    }
}
