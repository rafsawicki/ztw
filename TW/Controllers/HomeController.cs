﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TW.Models.Database;
using TW.Services;

namespace TW.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserProfileService _userProfileService;

        public HomeController() {
            _userProfileService = new UserProfileService();
        }

        public ActionResult Index(string query = "")
        {
            IList<Lesson> lessons = LessonService.GetNewestThree();
            ViewBag.lessons = lessons;
            IList<News> news = NewsService.GetNewestSix();
            ViewBag.news = news;
            
            if (query != "")
                return Search(query);
            if (Request.IsAjaxRequest()) return PartialView();
            return View();
        }

        public ActionResult UserSidePanel() {
            return PartialView("_UserBox", _userProfileService.GetCurrentUserProfileDetails());
        }

        public ActionResult Search(string query)
        {
            IList<Lesson> lessons = Services.LessonService.Search(query);
            ViewBag.lessons = lessons;
            return View("_Search");
        }

/*        //
        // GET: /Home/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Home/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Home/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Home/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Home/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Home/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Home/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }*/
    }
}
