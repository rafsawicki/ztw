﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TW.Common;
using TW.Constants;
using TW.Filters;
using TW.Models.ViewModels;
using TW.Services.Account;

namespace TW.Controllers {
    public class AccountController : Controller {
        private IFormsAuthenticationService _formsAuthenticationService;

        public AccountController() {
            _formsAuthenticationService = new FormsAuthenticationService();
        }

        public ActionResult SignIn(string view) {
            ViewBag.View = view ?? "_LoginForm";
            if (Request.IsAjaxRequest()) return PartialView("SignIn");
            return View("SignIn");
        }

        public ActionResult LoginForm() {
            if (Request.IsAjaxRequest()) return PartialView("_LoginForm");
            return SignIn("_LoginForm");
        }

        public ActionResult RegisterForm() {
            if (Request.IsAjaxRequest()) return PartialView("_RegisterForm");
            return SignIn("_RegisterForm");
        }

        [HttpPost]
        public JsonResult Login(LoginModel loginModel) {
            ModelState.Merge(_formsAuthenticationService.ValidateLoginModel(loginModel), "");
            if (ModelState.IsValid) {
                _formsAuthenticationService.UserSignIn(loginModel);
                return Json(JsonResponseFactory.SuccessResponse(Url.Action("Index", "Home")));
            }
            return
                Json(JsonResponseFactory.ErrorResponse(JsonHelper.GetModelErrorMessages(ModelState)));
        }

        [HttpPost]
        public JsonResult Register(RegisterModel registerModel) {
            ModelState.Merge(_formsAuthenticationService.ValidateRegisterModel(registerModel), "");
            if (ModelState.IsValid) {
                _formsAuthenticationService.RegisterUser(registerModel);
                _formsAuthenticationService.UserSignIn(new LoginModel() {
                    Username = registerModel.Username,
                    Password = registerModel.Password,
                    RememberMe = false
                });
                return Json(JsonResponseFactory.SuccessResponse(Url.Action("Index", "Home")));
            }
            return
                Json(JsonResponseFactory.ErrorResponse(JsonHelper.GetModelErrorMessages(ModelState)));
        }

        public ActionResult Authentication(Constants.AuthenticationType authType) {
            String authString = Url.Action("AuthenticationResult", "Account", new { authType = authType.ToString() }, "http");
            Uri authUrl = new Uri(authString);

            RequestAuthentication(authType, authUrl);

            //should never reach this point
            return new EmptyResult();
        }

        /// <summary>
        /// Requests the authentication from various openid and oauth services. Should redirect the user within th DotNetOpenAuth Dll.
        /// </summary>
        /// <param name="authType">Type of the auth.</param>
        /// <param name="authUrl">The redirect url, used after the service has authenticated the user.</param>
        private void RequestAuthentication(Constants.AuthenticationType authType, Uri authUrl) {

            if (authType == Constants.AuthenticationType.Facebook) {
                var auth = AuthenticationHelper.AuthFacebookClient();
                auth.RequestAuthentication(HttpContext, authUrl);
            } else if (authType == Constants.AuthenticationType.Google) {
                var auth = new DotNetOpenAuth.AspNet.Clients.GoogleOpenIdClient();
                auth.RequestAuthentication(HttpContext, authUrl);
            } else if (authType == Constants.AuthenticationType.Microsoft) {
                var auth = AuthenticationHelper.AuthMicrosoftClient();
                auth.RequestAuthentication(HttpContext, authUrl);
            } else if (authType == Constants.AuthenticationType.Twitter) {
                var auth = AuthenticationHelper.AuthTwitterClient();
                auth.RequestAuthentication(HttpContext, authUrl);
            } else if (authType == Constants.AuthenticationType.Yahoo) {
                var auth = new DotNetOpenAuth.AspNet.Clients.YahooOpenIdClient();
                auth.RequestAuthentication(HttpContext, authUrl);
            }/* else {
                var auth = new DotNetOpenAuth.AspNet.Clients.OpenIdClient("OpenId", openid_identifier);
                auth.RequestAuthentication(HttpContext, authUrl);
            }*/
        }

        [HttpGet]
        public ActionResult AuthenticationResult(Constants.AuthenticationType authType) {
            DotNetOpenAuth.AspNet.AuthenticationResult res;

            String authString = Url.Action("AuthenticationResult", "Account", new { authType = authType.ToString() }, "http");
            Uri authUrl = new Uri(authString);

            VerifyAuthentication(authType, out res, authUrl);

            if (!res.IsSuccessful) {
                throw new Exception("Authentication Provider Error: Sorry, it seems that an error occured while validating your Authentication Provider's response.");
            } else {
                _formsAuthenticationService.WebIdentitySignIn(res, true);
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Verifies the authentication from the various OpenId and OAuth services, returns generic Authetnication Result in res variable.
        /// </summary>
        /// <param name="authType">Type of the auth.</param>
        /// <param name="res">The res.</param>
        private void VerifyAuthentication(Constants.AuthenticationType authType, out DotNetOpenAuth.AspNet.AuthenticationResult res, Uri authUrl) {
            if (authType == Constants.AuthenticationType.Facebook) {
                var validate = AuthenticationHelper.AuthFacebookClient();
                res = validate.VerifyAuthentication(HttpContext, authUrl);
            } else if (authType == Constants.AuthenticationType.Google) {
                var validate = new DotNetOpenAuth.AspNet.Clients.GoogleOpenIdClient();
                res = validate.VerifyAuthentication(HttpContext);
            } else if (authType == Constants.AuthenticationType.Microsoft) {
                var validate = AuthenticationHelper.AuthMicrosoftClient();
                res = validate.VerifyAuthentication(HttpContext);
            } else if (authType == Constants.AuthenticationType.Twitter) {
                var validate = AuthenticationHelper.AuthTwitterClient();
                res = validate.VerifyAuthentication(HttpContext);
            } else if (authType == Constants.AuthenticationType.Yahoo) {
                var validate = new DotNetOpenAuth.AspNet.Clients.YahooOpenIdClient();
                res = validate.VerifyAuthentication(HttpContext);
            } else {
                var validate = new DotNetOpenAuth.AspNet.Clients.OpenIdClient("OpenId", "");
                res = validate.VerifyAuthentication(HttpContext);

            }
        }

        public ActionResult SignOut() {
            _formsAuthenticationService.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}

#region LEGACY_CODE
/*        private static readonly OpenIdRelyingParty Openid = new OpenIdRelyingParty();
        private static readonly FacebookClient FbClient = new FacebookClient {
            ClientIdentifier = "152431498269721",
            ClientCredentialApplicator =
              ClientCredentialApplicator
                .PostParameter("8be9e41d266627d4d3c453833798f486")
        };

        /// <summary>
        /// Authentication/Login post action.
        /// Original code concept from 
        /// DotNetOpenAuth/Samples/OpenIdRelyingPartyMvc/Controller/UserController
        /// for demo purposes I took out the returnURL, for this demo I 
        /// always want to login to the home page.
        /// </summary>
        [ValidateInput(false)]
        public ActionResult LoginOpenId() {
            var response = Openid.GetResponse();

            var statusMessage = "";
            if (response == null) {
                string openIdIdentifier = "https://www.google.com/accounts/o8/id";
                Identifier id;
                //make sure your users openid_identifier is valid.
//                if (Identifier.TryParse(Request.Form["openid_identifier"], out id)) {
                if (Identifier.TryParse(openIdIdentifier, out id)) {
                    var request = Openid.CreateRequest(openIdIdentifier);

                    var fetch = new FetchRequest();
                    fetch.Attributes.Add(new AttributeRequest(WellKnownAttributes.Contact.Email, true));
                    fetch.Attributes.Add(new AttributeRequest(WellKnownAttributes.Name.First, true));
                    fetch.Attributes.Add(new AttributeRequest(WellKnownAttributes.Name.Last, true));

                    request.AddExtension(fetch);

                    try {
                        //request openid_identifier
                        return request.RedirectingResponse.AsActionResult();
                    } catch (ProtocolException ex) {
                        statusMessage = ex.Message;
                        return RedirectToAction("Index", "Home");
                    }
                } else {
                    statusMessage = "Invalid identifier";
                    return RedirectToAction("Index", "Home");
                }
            } else {
                //check the response status
                switch (response.Status) {
                    //success status
                    case AuthenticationStatus.Authenticated:
                        Session["UserDisplayableIdentifier"] = response.FriendlyIdentifierForDisplay;

                        var fetch = response.GetExtension<FetchResponse>();
                        if (fetch != null) {
                            IList<string> emailAddresses =fetch.Attributes[WellKnownAttributes.Contact.Email].Values;
                            IList<string> firstNames = fetch.Attributes[WellKnownAttributes.Name.First].Values;
                            IList<string> lastNames = fetch.Attributes[WellKnownAttributes.Name.Last].Values;
                            Session["UserDisplayableIdentifier"] = firstNames.FirstOrDefault() + " " +
                                                               lastNames.FirstOrDefault();
                        }


                        FormsAuthentication.SetAuthCookie(response.ClaimedIdentifier, false);

                        //TODO: response.ClaimedIdentifier, to login or create new account 

                        return RedirectToAction("Index", "Home");

                    case AuthenticationStatus.Canceled:
                        statusMessage = "Canceled at provider";
                        return RedirectToAction("Index", "Home");

                    case AuthenticationStatus.Failed:
                        statusMessage = response.Exception.Message;
                        return RedirectToAction("Index", "Home");
                }
            }
            return new EmptyResult();
        }

        public ActionResult LoginFacebook() {
            IAuthorizationState authorization = FbClient.ProcessUserAuthorization();
            if (authorization == null) {
                // Kick off authorization request
                FbClient.RequestUserAuthorization();
            } else {
                var request = WebRequest.Create(
                  "https://graph.facebook.com/me?access_token="
                  + Uri.EscapeDataString(authorization.AccessToken));
                using (var response = request.GetResponse()) {
                    using (var responseStream = response.GetResponseStream()) {
                        var graph = FacebookGraph.Deserialize(responseStream);

                        string myUserName = graph.Name;
                        string myEmail = graph.Email;
                        // use the data in the graph object to authorise the user
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }*/
#endregion
