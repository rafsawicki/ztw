﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TW.Common;
using TW.Models;
using TW.Models.Database;
using TW.Services;

namespace TW.Controllers
{
    public class ExerciseController : Controller
    {
        public ActionResult Details(int id = 0, int exercise = 0)
        {
            Lesson lesson = null;
            IList<Exercise> exercises = null;
            lesson = Services.LessonService.GetLesson(id);
            exercises = Services.LessonService.getExercisesFromLesson(id);
            if (exercises.Count > 0)
            {
                ViewBag.exercises = exercises;
                ViewBag.current = exercise;
                ViewBag.exeNumb = exercises.Count;
                ViewBag.lesson = lesson;
            }
            ViewBag.View = "_Page";
            if (Request.IsAjaxRequest())
            {
                if (ViewBag.exercises != null && ViewBag.lesson != null) return PartialView("_Page");
                Response.StatusCode = 404;
                return Json(JsonResponseFactory.SuccessResponse("Podana lekcja nie posiada obecnie zadań"));
            }
            return View("_Details");
        }

        public ActionResult Index(string view, int id = 0, int page = 1)
        {
            Lesson lesson = null;
            IList<Page> pages = null;
            lesson = Services.LessonService.GetLesson(id);
            pages = Services.LessonService.getPagesFromLesson(id);
            if (pages.Count > 0)
            {
                ViewBag.lesson = lesson;
                ViewBag.page = pages[page];
                ViewBag.pagesNumb = pages.Count;
                ViewBag.currentPage = page;
            }
            ViewBag.View = "_Page";
            if (pages.Count == 0) {
                return RedirectToAction("Index", "Category");
            }
            if (Request.IsAjaxRequest()) return PartialView("_Details");
            return View("_Details");
        }

        public ActionResult Save(int id, string save)
        {
            string[] expes = save.Split(Convert.ToChar(";"));
            int[] exp = new int[100000];
            foreach (string expe in expes)
            {
                string[] parse = expe.Split(Convert.ToChar(","));
                if(parse.Length > 1)
                    exp[Convert.ToInt32(parse[0])] = Convert.ToInt32(parse[1]);
            }
            int expSum = 0;
            foreach (int intse in exp)
            {
                expSum += intse;
            }
            Services.UserProfileService.SaveUserProgres(expSum, id);
            return Json(JsonResponseFactory.SuccessResponse(Url.Action("Index", "Home")));
        }

        /*//
        // GET: /Lesson/

        public ActionResult DisplayLesson()
        {
            Lesson l = new Lesson();
            l.AddDate = new DateTime(2012, 10, 10);
            l.Content = "test";
            return View("DisplayLesson", l);
//            return View("DisplayLesson");
        }*/

//        public ActionResult AddLess
    }
}
