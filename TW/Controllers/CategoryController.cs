﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TW.Models.Database;

namespace TW.Controllers
{
    public class CategoryController : Controller
    {
        //
        // GET: /Category/

        public ActionResult Index( string view, int id = 0)
        {
            IList<Category> categories =  Services.CategoryService.GetAll();
            int[] nodesWithDepth = new int[categories.Max(category1 => category1.Id)+1];
            foreach (Category category in categories)
            {
                int counter = 0;
                foreach (Category category2 in categories)
                {
                    if (category2.Lft < category.Lft && category2.Rgt > category.Rgt)
                        counter++;
                }
                nodesWithDepth[category.Id] = counter;
            }
            ViewBag.categories = categories;
            ViewBag.order = nodesWithDepth;
            ViewBag.View = view ?? "_Details";
            IList<Lesson> lessons = null;
            if (id == 0)
            {
                lessons = Services.LessonService.GetAllLessons();
            }
            else
            {
                lessons = Services.LessonService.GetAllFromCategory(id);
            }
            if (lessons.Count > 0) {
                ViewBag.firstLesson = lessons.First();
                lessons.Remove(lessons.First());
                ViewBag.lessons = lessons; 
            }
            if (Request.IsAjaxRequest()) return PartialView("_Index");
            return View("_Index");
        }


//         GET: /Category/Details/5

        public ActionResult Details(int id = 0)
        {
            IList<Lesson> lessons = null;
            if(id == 0)
            {
                lessons = Services.LessonService.GetAllLessons();
            }
            else
            {
                lessons = Services.LessonService.GetAllFromCategory(id);
            }
            if (lessons.Count > 0) {
                ViewBag.firstLesson = lessons.First();
                lessons.Remove(lessons.First());
                ViewBag.lessons = lessons; 
            }
            if (Request.IsAjaxRequest()) {
                if (ViewBag.firstLesson != null) return PartialView("_Details");
                return Content("Wybrana kategoria jest pusta.");
            }
            return Index("_Details", id);
        }

        public ActionResult SignIn(string view)
        {
            ViewBag.View = view ?? "_LoginForm";
            if (Request.IsAjaxRequest()) return PartialView("SignIn");
            return View("SignIn");
        }

        public ActionResult LoginForm()
        {
            if (Request.IsAjaxRequest()) return PartialView("_LoginForm");
            return SignIn("_LoginForm");
        }

        public ActionResult RegisterForm()
        {
            if (Request.IsAjaxRequest()) return PartialView("_RegisterForm");
            return SignIn("_RegisterForm");
        }

        
//         GET: /Category/Create

        public ActionResult Create()
        {
            return View();
        }

        
//         POST: /Category/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        
//         GET: /Category/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        
//         POST: /Category/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        
//         GET: /Category/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        
//         POST: /Category/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
