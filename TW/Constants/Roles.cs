﻿using System;

namespace TW.Constants {
    [Flags]
    public enum Roles : long {
//        All roles must be powers of 2
        None = 0,
        Student = 1 << 0,
        Teacher = 1 << 1
    }
}