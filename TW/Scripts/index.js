﻿$(document).ready(function () {
    navbarActiveUpdate();
    bindAjaxLink('.navbar-ajax-link', document, '#content-panel', 200, 300);
    bindAjaxLink('.user-box-link', document, '#content-panel', 200, 300);
    registerGlobalAjaxHandlers();
    userBoxToggle();
    $('.changeNews').on('click', function (ev) {
        var id = $(this).attr('id');
        $(".mainNewsDivs").each(function () {
            $(this).hide();
        });
        $('#news' + id).show();

    });
    $('#searchFieldHome').keypress(function (e) {
        $(this).val($(this).val()+$(this).text());
    });
    $('.icon-search').on('click', document, function (e) {
        if ($('#searchFieldHome').val() != "") {
            var url = $('#searchFieldHome').val();
            $.get('/Home/Search?query=' + $('#searchFieldHome').val(), function (data) {
                $('#content-panel').fadeOut(200);
                $('#content-panel').promise().done(
                    function () {
                        $('#content-panel').html(data);
                        prepareLoadedForm('#content-panel');
                        $('#content-panel').fadeIn(300);
                    });
            });
            history.pushState(null, null, url);
        } else {
            console.log('Empty');
        }

        //        $('#searchFieldHome').val("");
    });
});

function bindAjaxLink(linkIdentifier, bindNodeIdentifier, contentIdentifier, fadeOutTime, fadeInTime) {
    $(bindNodeIdentifier).on('click', linkIdentifier, function (ev) {
        ev.preventDefault();
        var url = $(this).data('url');
        loadConetnt(url, contentIdentifier, fadeOutTime, fadeInTime);
        history.pushState(null, null, url);
    });
}

function loadConetnt(url, contentIdentifier, fadeOutTime, fadeInTime) {
    $.post(url, function(data) {
        $(contentIdentifier).fadeOut(fadeOutTime);
        $(contentIdentifier).promise().done(
            function() {
                $(contentIdentifier).html(data);
                prepareLoadedForm(contentIdentifier);
                $(contentIdentifier).fadeIn(fadeInTime);
            });
    });
}

function onAjaxFormSubmit(formIdentifier, bindNodeIdentifier, onSuccess, onFailure) {
    $(bindNodeIdentifier).on('submit', formIdentifier, function (ev) {
        ev.preventDefault();

        if ($(this).valid()) {
            var formObject = $(this).serialize();
            var url = $(this).attr('action');
            $.post(url, formObject, function(data) {
                if (data.Success == true) onSuccess(data);
                else onFailure(data);
            });
        }
    });
}

function prepareLoadedForm(id) {
    var $form = $(id).find('form');
    if ($form.length) {
        $form.unbind();
        $form.data('validator', null);
        $.validator.unobtrusive.parse(document);
    }
}

function navbarActiveUpdate() {
    $('.navbar-link').click(function () {
        var $li = $('.navbar').find('li');
        if ($li.length) {
            $li.removeClass('active');
        }
        $(this.parentNode).addClass('active');
    });
    var $li2 = $('.navbar').find('li');
    if ($li2.length) {
        $li2.removeClass('active');
        for (var i = 0; i < $li2.length; i++) {
            if ($($li2[i]).find('.navbar-link').data('url') == document.location.pathname) $($li2[i]).toggleClass('active', true);
        }
    }
}

function registerGlobalAjaxHandlers() {
    $.ajaxSetup({
        statusCode: {
            401: function () {
                $("#global-error-panel").fadeOut(200);
                $("#global-error-panel").promise().done(
                function () {
                    $("#global-error-panel").html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>Nie masz uprawnień do wykonania tej operacji.</div>');
                    $("#global-error-panel").fadeIn(300);
                });
            },
            404: function () {
                $("#global-error-panel").fadeOut(200);
                $("#global-error-panel").promise().done(
                function () {
                    $("#global-error-panel").html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>Podana strona nie posiada treści.</div>');
                    $("#global-error-panel").fadeIn(300);
                });
            },
            500: function () {
                $("#global-error-panel").fadeOut(200);
                $("#global-error-panel").promise().done(
                function () {
                    $("#global-error-panel").html('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>Wystąpił błąd serwera. Spróbuj ponownie później.</div>');
                    $("#global-error-panel").fadeIn(300);
                });
            }
        }
    });
    $(document).ajaxStart(function() {
        $('#ajax-busy-indicator').clearQueue().fadeOut(0).delay(1000).fadeIn(0);
    });
    $(document).ajaxStop(function() {
        $('#ajax-busy-indicator').clearQueue().fadeOut(0);
    });
}

function userBoxToggle() {
    $('#user-box-bar .user-details-toggle').click(function () {
        $(this).toggleClass('icon-arrow-right');
        $(this).toggleClass('icon-arrow-left');
        $('#user-box-position-wrapper').toggleClass('user-details-visible');
        $('#user-box-position-wrapper').toggleClass('user-details-hidden');
        $('#user-box-details').toggle();
        localStorage.userBoxVisible = $("#user-box-position-wrapper").hasClass('user-details-visible');
        if (window.opera) {
            document.body.style += "";
            document.body.style -= "";
        }
    });

    if (localStorage.userBoxVisible === "false") {
        $('#user-box-bar .user-details-toggle').trigger('click');
    }
    $('#user-box-position-wrapper').removeClass('user-box-hidden');
}