﻿$(document).ready(function () {
    $("#content-panel").on("click", ".categoryName", function () {
        $(this).parent().find("div").toggle();
    });

    bindAjaxLink('.showCategoryLink', '#content-panel', '#lessonsContent', 200, 300);
    bindAjaxLink('.categoryGoToLesson', document, '#content-panel', 200, 300);
});