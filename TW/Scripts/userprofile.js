﻿var pageCounter = 1;

$(document).ready(function () {
    bindAjaxLink('.profile-ajax-link', '#content-panel', '#user-profile-content-panel', 200, 300);
    onAjaxFormSubmit('.update-profile-form', '#content-panel',
        function(data) {
             profileFormSuccess('#form-details-message-panel', data);
            location.reload();
        },
        function (data) { profileFormFailure('#form-details-message-panel', data); });
    onAjaxFormSubmit('.update-password-form', '#content-panel',
        function (data) { profileFormSuccess('#form-password-message-panel', data); },
        function (data) { profileFormFailure('#form-password-message-panel', data); });
    initFileUpload();
    initFormEditor();
    initTabsUpdate();
    initAchievementLinks();
    bindProfileLink('.lesson-ajax-link', '#content-panel', '#user-profile-content-panel', 200, 300, function() {
        transformToEditor('#lesson-page1 textarea');
    });
    bindProfileLink('.lesson-ajax-link', '#content-panel', '#user-profile-content-panel', 200, 300, function() {
        transformToEditor('#lesson-page1 textarea');
    });
    bindProfileLink('.user-box-profile-edit-ajax-link', document, '#content-panel', 200, 300, function() {
        initFileUpload();
        initTabsUpdate();
    });
    bindProfileLink('.profile-edit-ajax-link', document, '#user-profile-content-panel', 200, 300, function() {
        initFileUpload();
    });
});

function initAchievementLinks() {
    $('#content-panel').on('click', '.profile-achievement-link', function(ev) {
        ev.preventDefault();
        $('#selected-achievement-panel').fadeOut(200);
        $('#selected-achievement-panel').promise().done(
            function() {
                $('#selected-achievement-panel .image-panel').html('<img src="' + $(ev.target).attr('src') + '" width="250" height = "250" alt=""/>');
                $('#selected-achievement-panel .content-panel').html('<h3>' + $(ev.target).parent().data('name') + '</h3><p>' + $(ev.target).parent().data('description') + ' </p>');
                $('#selected-achievement-panel').fadeIn(300);
            });
        
    });
}

function initTabsUpdate() {
    var $li2 = $('#profile-nav-tabs').find('li');
    if ($li2.length) {
        $li2.removeClass('active');
        for (var i = 0; i < $li2.length; i++) {
            if ($($li2[i]).find('a').data('url') == document.location.pathname) $($li2[i]).toggleClass('active', true);
        }
    }
}

function bindProfileLink(linkIdentifier, bindNodeIdentifier, contentIdentifier, fadeOutTime, fadeInTime, onAjaxFinished) {
    $(bindNodeIdentifier).on('click', linkIdentifier, function (ev) {
        ev.preventDefault();
        var url = $(this).data('url');
        loadProfileConetnt(url, contentIdentifier, fadeOutTime, fadeInTime, onAjaxFinished);
        history.pushState(null, null, url);
    });
}

function loadProfileConetnt(url, contentIdentifier, fadeOutTime, fadeInTime, onAjaxFinished) {
    $.post(url, function(data) {
        $(contentIdentifier).fadeOut(fadeOutTime);
        $(contentIdentifier).promise().done(
            function() {
                $(contentIdentifier).html(data);
                prepareLoadedForm(contentIdentifier);
                $(contentIdentifier).fadeIn(fadeInTime);
                onAjaxFinished();
            });
    });
}

function profileFormSuccess(identifier, data) {
    $(identifier).append($('<div class="alert alert-success"><a class="close" data-dismiss="alert">×</a>Operacja zakończona sukcesem.</div>'));
}

function profileFormFailure(identifier, data) {
    $(identifier).fadeOut(200);
    $(identifier).promise().done(function () {
        $(identifier).empty();
        var errors = data.Object;
        for (var i = 0; i < errors.length; i++) {
            $(identifier).append($('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>' + errors[0] + '</div>'));
        }
        $(identifier).fadeIn(300);
    });
}


function initFileUpload() {
    $('#dropZone').filedrop({
        url: '/UserProfile/UploadFiles',
        paramname: 'files',
        maxFiles: 1,
        dragOver: function() {
            $('#dropZone').css('background', '#CCCCCC');
        },
        dragLeave: function() {
            $('#dropZone').css('background', '#F7F7F9');
        },
        drop: function() {
            $('#dropZone').css('background', '#F7F7F9');
        },
        afterAll: function() {
            $('#dropZone').html('Plik został wysłany!');
            location.reload();
        },
    });
}

function initFormEditor() {
    transformToEditor('#lesson-page1 textarea');
    $('#content-panel').on('click', '#add-lesson-page', function(ev) {
        ev.preventDefault();
        pageCounter++;
        $('#add-lesson-panel .tab-pane').hide();
        var pageName = '<div class="control-group"><label class="control-label" for="PageName">Nazwa strony<span class="required" style="visibility:hidden;">*</span></label><div class="controls"><input id="PageName" name="PageName" type="text" value=""><span class="help-inline"><span class="field-validation-valid" data-valmsg-for="PageName" data-valmsg-replace="true"></span></span></div></div>';
        $('#add-lesson-panel form').append('<div class="tab-pane active" id="lesson-page' + pageCounter + '">'+pageName+'<textarea name="content" style="width:95%"></textarea></div>');
        transformToEditor('#lesson-page'+pageCounter+' textarea');
        $('#lesson-page-list .active').removeClass('active');
        var el = '<li class="active"><a href="#" data-toggle="tab" class="lesson-editor-page-link" data-target="#lesson-page' + pageCounter + '">Strona ' + pageCounter +'</a></li>';
        $('#add-lesson-page').parent().before(el);
    });
    $('#content-panel').on('click', '.lesson-editor-page-link', function(ev) {
        ev.preventDefault();
        $('#add-lesson-panel .tab-pane').hide();
        $($(this).data('target')).show();
    });
}

function transformToEditor(selector) {
        tinymce.init({
    selector: selector,
    theme: "modern",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    });
}

/*function initImageUpload() {
    $('#dropzone').filedrop({
//    fallback_id: 'upload_button',   // an identifier of a standard file input element
    url: 'upload.php',              // upload handler, handles each file separately, can also be a function returning a url
    paramname: 'userfile',          // POST parameter name used on serverside to reference file
    withCredentials: true,          // make a cross-origin request with cookies
    data: {
        param1: 'value1',           // send POST variables
        param2: function(){
            return calculated_data; // calculate data at time of upload
        },
    },
    headers: {          // Send additional request headers
        'header': 'value'
    },
    error: function(err, file) {
        switch(err) {
            case 'BrowserNotSupported':
                alert('browser does not support html5 drag and drop')
                break;
            case 'TooManyFiles':
                // user uploaded more than 'maxfiles'
                break;
            case 'FileTooLarge':
                // program encountered a file whose size is greater than 'maxfilesize'
                // FileTooLarge also has access to the file which was too large
                // use file.name to reference the filename of the culprit file
                break;
            case 'FileTypeNotAllowed':
                // The file type is not in the specified list 'allowedfiletypes'
            default:
                break;
        }
    },
    allowedfiletypes: ['image/jpeg','image/png','image/gif'],   // filetypes allowed by Content-Type.  Empty array means no restrictions
    maxfiles: 25,
    maxfilesize: 20,    // max file size in MBs
    dragOver: function() {
        // user dragging files over #dropzone
    },
    dragLeave: function() {
        // user dragging files out of #dropzone
    },
    docOver: function() {
        // user dragging files anywhere inside the browser document window
    },
    docLeave: function() {
        // user dragging files out of the browser document window
    },
    drop: function() {
        // user drops file
    },
    uploadStarted: function(i, file, len){
        // a file began uploading
        // i = index => 0, 1, 2, 3, 4 etc
        // file is the actual file of the index
        // len = total files user dropped
    },
    uploadFinished: function(i, file, response, time) {
        // response is the data you got back from server in JSON format.
    },
    progressUpdated: function(i, file, progress) {
        // this function is used for large files and updates intermittently
        // progress is the integer value of file being uploaded percentage to completion
    },
    globalProgressUpdated: function(progress) {
        // progress for all the files uploaded on the current instance (percentage)
        // ex: $('#progress div').width(progress+"%");
    },
    speedUpdated: function(i, file, speed) {
        // speed in kb/s
    },
    rename: function(name) {
        // name in string format
        // must return alternate name as string
    },
    beforeEach: function(file) {
        // file is a file object
        // return false to cancel upload
    },
    beforeSend: function(file, i, done) {
        // file is a file object
        // i is the file index
        // call done() to start the upload
    },
    afterAll: function() {
        // runs after all files have been uploaded or otherwise dealt with
    }
});
}*/