﻿$(document).ready(function () {
    bindAjaxLink('.sign-in-ajax-link', '#content-panel', '#login-register-form', 200, 300);
    onAjaxFormSubmit('.sign-in-form', '#content-panel', signInFormSuccess, signInFormFailure);
});

function signInFormSuccess(data) {
    window.location.replace(data.Object);
}

function signInFormFailure(data) {
    $('#form-error-panel').fadeOut(200);
    $('#form-error-panel').promise().done(function () {
        $('#form-error-panel').empty();
        var errors = data.Object;
        for (var i = 0; i < errors.length; i++) {
            $('#form-error-panel').append($('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a>' + errors[0] + '</div>'));
        }
        $('#form-error-panel').fadeIn(300);
    });
}