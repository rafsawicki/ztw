﻿$(document).ready(function () {
    $('#exerciseHelp').on('click', function (ev) {
        $('#showExerciseHelp').show();
    });
    $('#content-panel').on('click', '#ExerciseSaveProgress', function (ev) {
        var url = "/Exercise/Save/" + $('#exerciseInfoLesson').attr('value')+"?save="+localStorage[$('#exerciseInfoLesson').attr('value')];
        $.ajax({
            type: "POST",
            url: url,
        }).done(function( msg ) {
            console.log('saved');
            localStorage[$('#exerciseInfoLesson').attr('value')] = '';
            window.location = ('/Lesson/Details/' + $('#exerciseInfoLesson').attr('value'));
        });
    });
    $('#content-panel').on('click', '.exerciseCheckAnswer', function (ev) {
        console.log($(this));
        if ($('#exerciseAnswer').attr('value') == $('#Answer').val()) {
            $('#answerRight').show();
            $('#answerWrong').hide();
            if (localStorage[$('#exerciseInfoLesson').attr('value')] == undefined) {
                localStorage[$('#exerciseInfoLesson').attr('value')] = $('#exerciseInfo').attr('value') + "," + $('#exerciseInfoExp').attr('value') + ";";
            } else {
                localStorage[$('#exerciseInfoLesson').attr('value')] += $('#exerciseInfo').attr('value') + "," + $('#exerciseInfoExp').attr('value') + ";";
            }
            $('#iconEx' + $(this).attr('value')).removeClass('badge');
            $('#iconEx' + $(this).attr('value')).removeClass('badge-success');
            $('#iconEx' + $(this).attr('value')).removeClass('badge-important');
            $('#iconEx' + $(this).attr('value')).toggleClass('badge badge-success');
            $('#iconEx' + $(this).attr('value')).html("&#10004;");
        } else {
            $('#answerWrong').show();
            $('#answerRight').hide();
            if (localStorage[$('#exerciseInfoLesson').attr('value')] == undefined) {
                localStorage[$('#exerciseInfoLesson').attr('value')] = $(this).attr('value') + ",0;";
            } else {
                localStorage[$('#exerciseInfoLesson').attr('value')] += $(this).attr('value') + ",0;";
            }

            $('#iconEx' + $(this).attr('value')).removeClass('badge');
            $('#iconEx' + $(this).attr('value')).removeClass('badge-success');
            $('#iconEx' + $(this).attr('value')).removeClass('badge-important');
            $('#iconEx' + $(this).attr('value')).toggleClass('badge badge-important');
            $('#iconEx' + $(this).attr('value')).html("!");
        }
    });
    $('#content-panel').on('click', '.exerciseChangeExercise', function (ev) {
        ev.preventDefault();
        var url = $($(this)).data('url');
        loadConetnt(url, '#exerciseContent', 200, 300);
        history.pushState(null, null, url);
    });
});