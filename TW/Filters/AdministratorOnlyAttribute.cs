﻿using System;
using System.Web;
using System.Web.Mvc;
using TW.Services.Account;
using TW.Common;

namespace TW.Filters {
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, Inherited = false)]
    public class AdministratorOnlyAttribute : ActionFilterAttribute, IAuthorizationFilter {
        public void OnAuthorization(AuthorizationContext filterContext) {
            if (filterContext.HttpContext.Session == null) return;
            var success = filterContext.HttpContext.User.UserSession().IsAdministrator;

            if (success) {
                // Since authorization is performed at the action level, the authorization code runs
                // after the output caching module. In the worst case this could allow an authorized user
                // to cause the page to be cached, then an unauthorized user would later be served the
                // cached page. We work around this by telling proxies not to cache the sensitive page,
                // then we hook our custom authorization code into the caching mechanism so that we have
                // the final say on whether or not a page should be served from the cache.
                var cache = filterContext.HttpContext.Response.Cache;
                cache.SetProxyMaxAge(new TimeSpan(0));
                cache.AddValidationCallback((HttpContext context, object data, ref HttpValidationStatus validationStatus) => {
                                                    validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
                                                }, null);
            } else {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }

        private HttpValidationStatus OnCacheAuthorization(HttpContextBase httpContext) {
            if (httpContext.Session != null) {
                var success = httpContext.User.UserSession().IsAdministrator;
                return success ? HttpValidationStatus.Valid : HttpValidationStatus.IgnoreThisRequest;
            }
            return HttpValidationStatus.IgnoreThisRequest;
        }
    }
} 
