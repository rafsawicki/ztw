﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NHibernate;
using TW.Models.Database;

namespace TW.Common {
    public static class UserQueryExtensions {
        public static IQueryOver<User, User> WithIdentity(this IQueryOver<User, User> query, string identity) {
            return query.Where(user => user.Identity == identity);
        }
    }
}