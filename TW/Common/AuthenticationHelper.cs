﻿using System;
using System.Configuration;
using System.Security.Principal;
using System.Web;
using DotNetOpenAuth.AspNet.Clients;
using TW.Constants;
using TW.Models;
using TW.Models.Database;
using TW.Resources;

namespace TW.Common {
    public static class AuthenticationHelper {
        public static FacebookClient AuthFacebookClient() {
            String facebookKey =
            ConfigurationManager.AppSettings["AuthFacebookClientKey"];
            String facebookSecret =
            ConfigurationManager.AppSettings["AuthFacebookClientSecret"];
            return new FacebookClient(facebookKey, facebookSecret);
        }

        public static MicrosoftClient AuthMicrosoftClient() {
            String microsoftKey =
            ConfigurationManager.AppSettings["AuthMicrosoftClientKey"];
            String microsoftSecret =
            ConfigurationManager.AppSettings["AuthMicrosoftClientSecret"];
            return new MicrosoftClient(microsoftKey, microsoftSecret);
        }

        public static TwitterClient AuthTwitterClient() {
            String twitterKey =
            ConfigurationManager.AppSettings["AuthTwitterClientKey"];
            String twitterSecret =
            ConfigurationManager.AppSettings["AuthTwitterClientSecret"];
            return new TwitterClient(twitterKey, twitterSecret);
        }

        public static UserSession UserSession(this IPrincipal user) {
            if (HttpContext.Current.Session["UserSession"] == null) {
                if (string.IsNullOrEmpty(user.Identity.Name)) CreateDefaultUserSession();
                else CreateUserSession(user.Identity.Name);
            }
            return (UserSession)HttpContext.Current.Session["UserSession"];
        }

        public static void ReloadUserSession(this IPrincipal user) {
            HttpContext.Current.Session["UserSession"] = null;
        }

        private static void CreateDefaultUserSession() {
            HttpContext.Current.Session["UserSession"] = new UserSession() {
                Id = -1,
                Identity = "",
                DisplayableName = "Nieznajomy",
                IsAdministrator = false,
                Roles = Roles.None
            };
        }

        private static void CreateUserSession(string identity) {
            User user = null;
            SessionManager.InTransaction(session => user = session.QueryOver<User>().WithIdentity(identity).SingleOrDefault());
            if (user != null) {
                HttpContext.Current.Session["UserSession"] = new UserSession() {
                    Id = user.Id,
                    Identity = user.Identity,
                    DisplayableName = user.DisplayableName,
                    IsWebIdentity = user.IsWebIdentity,
                    IsAdministrator = user.IsAdministrator,
                    Roles = user.Roles
                };
            } else {
                throw new ApplicationException("Cannot create user session object for unknown user.");
            }
        }
    }
}