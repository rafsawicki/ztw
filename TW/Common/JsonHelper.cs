﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace TW.Common {
    public static class JsonHelper {
        public static IList GetModelErrorMessages(ModelStateDictionary modelState) {
            return modelState.Values.SelectMany(m => m.Errors).Select(e => e.ErrorMessage).ToList();
        }
    }
}