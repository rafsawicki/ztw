﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TW.Common {
    public static class HtmlHelpers {
        public static bool IsCurrentWebUser<TModel>(this HtmlHelper<TModel> source) {
            return HttpContext.Current.User.UserSession().IsWebIdentity;
        }
    }
}